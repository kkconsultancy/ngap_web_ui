import React from 'react';
import {Link} from 'react-router-dom';
const NavBar = (props) => {
    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link className="navbar-brand" to="#">NGAP</Link>
            <button 
                className="navbar-toggler"
                type="button" 
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="nav-link" to="/">Home</Link>
                    </li>
                    {/* <li className="nav-item">
                        <Link className="nav-link" to="#">Events</Link>
                    </li> */}
                    {/* <li className="nav-item"> */}
                    {/* <span className="sr-only">(current)</span> */}
                        {/* <Link className="nav-link" to="signin">Login</Link> */}
                    {/* </li> */}
                    <li className="nav-item dropdown visible">
                        <Link className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Username
                        </Link>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <Link className="dropdown-item" to="#">Change Password</Link>
                            <Link className="dropdown-item" to="#">Disable account</Link>
                            <Link className="dropdown-item" to="#">Delete account</Link>
                            <div className="dropdown-divider"></div>
                            <Link className="dropdown-item" to="#">Signout</Link>
                        </div>
                    </li>
                    {/* <li className="nav-item">
                        <Link className="nav-link disabled" to="#" tabindex="-1" aria-disabled="true">Disabled</Link>
                    </li> */} 
                </ul>
                {/* <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> */}
            </div>
        </nav>
    )
}

export default NavBar;