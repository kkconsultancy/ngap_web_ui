import React,{Component} from 'react';
import {Redirect} from 'react-router-dom'
import Presentation from './presentation'

class Container extends Component{
    state = {
        error:''
    }

    constructor(props,calls){
        super(props)
        this.props = props
    }

    stateSet = (id,message) => {
        this.setState({
            [id] : message
        })
    }

    callback = (action) => {
        this.setState({
            submitDisabled : false,
            resetDisabled : false
        });
        if(action.error)
            this.setState({error : action.code.split('/')[1] + " : " + action.message})
        else{
            this.setState({next : <Redirect to={"/"+action.next}/>})
        }
    }

    makeSignin = (response) => {
        const data = {
            email : response.data.email
        }
        this.props.resetPassword(data, this.callback);
    }

    formUpdate = (e) => {
        this.setState({[e.target.id] : e.target.value})
    }

    formSubmit = (e) => {
        e.preventDefault();

        this.stateSet("submitDisabled",true);
        this.stateSet("resetDisabled",true);

        const data = {
            username : this.state.username
        }

        if(!data.username){
            this.setState({error: "Enter Your Username!"})
            this.stateSet("submitDisabled", false);
            this.stateSet("resetDisabled", false);
            return;
        }

        const mailExp = /^.*@.*\..*/;
        const phoneExp = /^[0-9]+/;
        if (data.username.match(mailExp)) {
            this.makeSignin({data : {email : this.state.username}});
        } else if (data.username.match(phoneExp)) {
            this.props.fetchDetails.byPhoneNumber({phoneNumber : data.username},this.makeSignin);
        }else{
            this.props.fetchDetails.byId({uid : data.username}, this.makeSignin);
        }
    }

    render(){
        return(
            <Presentation
                {...this.state}
                formSubmit={this.formSubmit}
                formUpdate={this.formUpdate}
            />
        )
    }
}

export default Container;