import React,{Component} from 'react';
import {Redirect} from 'react-router-dom';
import Presentation from './presentation';

class Container extends Component{
    state = {
    }

    constructor(props){
        super(props)
        this.props = props;
    }

    stateSet = (id,message) => {
        this.setState({
            [id] : message
        })
    }

    callback = (action) => {
        this.setState({
            submitDisabled : false,
            resetDisabled : false
        });
        if(action.error)
            this.setState({ error: action.message})
        else{
            this.setState({
                error : '',
                next : <Redirect to={"/"+action.next}/>
            })
        }
    }


    formUpdate = (e) => {
        this.stateSet(e.target.id, e.target.value)

        if (e.target.id === "email") {
            const reg = /^.+@.+\..+/
            if (e.target.value.match(reg) === null) {
                this.stateSet("emailError", "Invalid Email");
            }
            else{
                this.stateSet("emailError", "");
            }
            return;
        }
    }

    formSubmit = (e) => {
        e.preventDefault();

        if(this.state.emailError)   return;

        const data = {
            email : this.state.email,
            password : this.state.password
        }

        this.stateSet("submitDisabled",true);
        this.stateSet("resetDisabled",true);
        
        this.props.signin(data, this.callback);
    }

    render(){
        // if(this.state.error) return(
        //     <Presentation
        //         {...this.state}
        //         formSubmit={this.formSubmit}
        //         formUpdate={this.formUpdate}
        //     />
        // )
        // else if(localStorage.getItem('kkuid')) return(
        //     <div className="container ">
        //         <div className="d-flex justify-content-center">
        //             <strong className="row badge-info">--- LOADING ---</strong>
        //         </div>
        //         {this.state.next}
        //     </div>
        // )
        // else 
        return (
            <Presentation
                {...this.state}
                formSubmit={this.formSubmit}
                formUpdate={this.formUpdate}
            />
        )
    }

}

export default Container;