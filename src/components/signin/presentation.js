import React from 'react'
import {Link} from 'react-router-dom'

const Presentation = props => (
    <div className="container">
        <h1>Login Form</h1>
        <div className="row badge badge-danger">{props.error}</div>
        <form onSubmit={props.formSubmit} className="container">
            <div className="row">
                <label>E-mail* :</label>
                <input 
                    type="text"
                    onChange={props.formUpdate}
                    id="email" 
                    className="form-control"
                />
            </div>
            <div className="row badge badge-danger">{props.emailError}</div>
            <div className="row">
                <label>Password* :</label>
                <input
                    type="password"
                    onChange={props.formUpdate}
                    id="password" 
                    className="form-control"
                />
            </div>
            <br/>
            <div className="row">
                <div className="col">
                    <button 
                        onClick={props.formSubmit}
                        disabled={props.submitDisabled}
                        className="btn btn-success btn-lg btn-block">
                        Login
                    </button>
                </div>
                <div className="col">
                    <button 
                        disabled={props.resetDisabled} 
                        type="reset"
                        className="btn btn-warning btn-lg btn-block">
                        Cancel
                    </button>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <h4><Link to="/forgotpassword"> Forgot Password</Link></h4>
                </div>
            </div>
            <div className="row">
                <div className="col"> 
                    <h4>No Account? <Link to="/register">SignUp</Link></h4>
                </div>
            </div>
        </form>
    </div>
)

export default Presentation