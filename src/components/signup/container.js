import React from 'react'
import {Redirect} from 'react-router-dom'
import Presentation from './presentation'

class Container extends React.Component{
    state = {
    }

    constructor(props){
        super(props)
        this.props = props;
    }

    stateSet = (id,message) => {
        this.setState({
            [id] : message
        })
    }

    callback = (action) => {
        this.setState({
            submitDisabled : false,
            resetDisabled : false
        })
        if(action.error)
            this.setState({error : action.message})
        else{
            this.setState({
                error : '',
                next : <Redirect to={"/"+ action.next}/>
            })
        }
    }

    formUpdate = (e) => {
        this.stateSet(e.target.id, e.target.value)

        if(e.target.id === "name"){
            const reg = /^[a-z,A-Z,\s]+$/
            if (e.target.value.match(reg) === null)
                this.stateSet("nameError", "Invalid Name")
            else    this.stateSet("nameError", "")
            return;
        }

        if(e.target.id === "regno"){
            const reg = /[0-9]{2}[A-Z][0-9][1,5]A[0-9]{2}[A-H,0-9][0-9]/
            if (e.target.value.match(reg) === null) 
                this.stateSet("regnoError", "Invalid University Regd.No.")
            else    this.stateSet("regnoError", "")
            return;
        }

        if (e.target.id === "email") {
            const reg = /^.+@.+\..+/
            if (e.target.value.match(reg) === null) {
                this.stateSet("emailError", "Invalid Email");
            }
            else{
                this.stateSet("emailError", "");
            }
            return;
        }

        if(e.target.id === "mobile"){
            if(!e.target.value.match(/^[0-9]{10}/))
            // if(!(e.target.value.length===10))
                this.stateSet("phoneError", "Invalid mobile number.");
            else{
                this.stateSet("phoneError", "");
            }
            return;
        }

        if (e.target.id === "password"){
            if (e.target.value !== this.state.cpassword) {
                this.stateSet("cpasswordError","Password missmatch");
            }
            else {
                this.stateSet("cpasswordError", "");
            }
            return;
        }
        if (e.target.id === "cpassword") {
            if (e.target.value !== this.state.password) {
                this.stateSet("cpasswordError", "Passwords missmatch");
            
            }
             else{
                this.stateSet("cpasswordError", "");
            }
            return;
        }
    }

    formSubmit = (e) => {
        e.preventDefault();

        if(this.state.emailError || this.state.phoneError || this.state.cpasswordError
            || this.state.nameError || this.state.regnoError)
            return;

        // console.log("submit")
        const data = {
            name : this.state.name,
            regNo : this.state.regno,
            password : this.state.password,
            email : this.state.email,
            phoneNo : this.state.mobile
        }

        this.stateSet("submitDisabled",true);
        this.stateSet("resetDisabled",true);

        this.props.signup(data,this.callback);
    }

    render(){
        return (
            <Presentation
                {...this.state}
                formSubmit = {this.formSubmit}
                formUpdate = {this.formUpdate}
            />
        )
    }

}

export default Container;