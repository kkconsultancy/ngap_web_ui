import Presentation from "./presentation";
import React from 'react'
import {Redirect} from 'react-router-dom'

class Container extends React.Component{
    state = {
        error:''
    }

    constructor(props,calls){
        super(props)
        this.props = props
        this.props.signout(this.callback);
    }

    callback = (action) => {
        if(action.error)
            this.setState({error : action.message})
        else{
            this.setState({next : <Redirect to={action.next}/>})
        }
    }

    render(){
        return (
            <Presentation 
                {...this.state}
            />
        )
    }
}

export default Container;