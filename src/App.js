import React,{Component} from 'react';
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom';

import Firebase from './middleware/index'

import Home from './components/home/';
import SignUp from './components/signup';
import SignIn from './components/signin';
import NavBar from './components/navBar';
import SignOut from './components/signout';
import Dashboard from './components/dashboard';
import ForgotPassword from './components/forgotpassword';
import EmailVerification from './components/emailverification';
import EmailReset from './components/emailreset';
import DisableAccount from './components/disableaccount'
import DeleteAccount from './components/deleteaccount'



class App extends Component {
  firebase = null;
  constructor() {
    super();
    this.firebase = new Firebase();
  }
  state = {
    firebaseReady: true,
    error: ''
  }
  callback = (id,value) => {
    this.setState({
      [id] : value
    });
  }
  render(){
    // if(this.state.error) return (
    //   <div className="container">
    //     <div className="d-flex justify-content-center">
    //       <div className="row badge badge-danger">{this.state.error}</div>
    //     </div>
    //   </div>
    // );
    // else if(!this.state.firebaseReady) return (
    //   <div className="container ">
    //     <div className="d-flex justify-content-center">
    //       <strong className="row badge-info">--- LOADING ---</strong>
    //     </div>
    //   </div>
    // );
    // else 
    return (
      <BrowserRouter className="App">
        <div><NavBar/></div><br/>
        <Route exact path="/" component={() => <Redirect to='/home' />} />
        <Route path="/home" component={() => <Home/>}/>
        <Route path="/dashboard" component={() => <Dashboard/>}/>
        {/* <Route path="/dashboard" component={() => <Dashboard user={this.firebase.state.auth.currentUser} signin={this.firebase.signin}/>}/> */}
        <Switch>
          <Route path="/login" component={() => <SignIn signin={this.firebase.signin} fetchDetails={{byId : this.firebase.getUserById,byEmail : this.firebase.getUserByEmail,byPhoneNumber : this.firebase.getUserByPhoneNumber}}/>} />
          <Route path="/signin" component={() => <SignIn signin={this.firebase.signin} fetchDetails={{byId: this.firebase.getUserById, byEmail: this.firebase.getUserByEmail, byPhoneNumber: this.firebase.getUserByPhoneNumber }} />} />
        </Switch>
        <Switch>
          <Route path="/register" component={() => <SignUp signup={this.firebase.signup} getUserById={this.firebase.getUserById}/>}/>
          <Route path="/signup" component={() => <SignUp signup={this.firebase.signup} getUserById={this.firebase.getUserById}/>}/>
        </Switch>
        <Switch>
          <Route path="/logout" component={() => <SignOut signout={this.firebase.signout} />} />
          <Route path="/signout" component={() => <SignOut signout={this.firebase.signout} />} />
        </Switch>
        <Switch>
          <Route path="/forgotpassword" component={() => <ForgotPassword resetPassword={this.firebase.resetPassword} fetchDetails={{ byId: this.firebase.getUserById, byEmail: this.firebase.getUserByEmail, byPhoneNumber: this.firebase.getUserByPhoneNumber }} />} />
          <Route path="/resetpassword" component={() => <ForgotPassword resetPassword={this.firebase.resetPassword} fetchDetails={{ byId: this.firebase.getUserById, byEmail: this.firebase.getUserByEmail, byPhoneNumber: this.firebase.getUserByPhoneNumber }} />} />
          <Route path="/changePassword" component={() => <ForgotPassword resetPassword={this.firebase.resetPassword} fetchDetails={{ byId: this.firebase.getUserById, byEmail: this.firebase.getUserByEmail, byPhoneNumber: this.firebase.getUserByPhoneNumber }} />} />
        </Switch>
        <Switch>
          <Route path="/disableAccount" component={() => <DisableAccount disableAccount={this.firebase.disableAccount} user={this.firebase.state.auth.currentUser}/>} />
          <Route path="/deactivateAccount" component={() => <DisableAccount disableAccount={this.firebase.disableAccount} user={this.firebase.state.auth.currentUser}/>} />
        </Switch>
        <Switch>
          <Route path="/deleteAccount" component={() => <DeleteAccount deleteAccount={this.firebase.deleteAccount} user={this.firebase.state.auth.currentUser}/>} />
          <Route path="/removeAccount" component={() => <DeleteAccount deleteAccount={this.firebase.deleteAccount} user={this.firebase.state.auth.currentUser}/>} />
        </Switch>
        <Route path="/emailVerification" component={EmailVerification}/>
        <Route path="/emailReset" component={EmailReset} />
      </BrowserRouter>
    );
  }
}

export default App;
